#include <stdio.h>

#define MAX_PARAMS 50
#define MAX_LINE_LENGTH 256

/* DATATYPES DEFINITIONS */

struct params
{
  char key[24];
  double value;
};

/* FUNCTION DEFINITIONS */

struct params *param_list_create (FILE *input);
double param_list_lookup (char *key, struct params *param_list);
void param_list_print (FILE *outfile, struct params *param_list);
void param_list_destroy (struct params *param_list);
double *read_file (char *file, size_t *row, size_t *column);
int get_size (const double *D, size_t r_D, size_t c_D, size_t *nX, size_t *nY);
int splice (const double *D, size_t c_D, size_t nX, size_t nY, double *K,
            double *V);

/* MACRO DEFINITIONS */
