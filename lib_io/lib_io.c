#include "lib_io.h"
#include "lib_io_internal.h"

/* READING PARAMETERS */

struct params *
param_list_create (FILE *input)
{
  char key[24] = { '\0' };
  double value = 0;
  char buffer[128], *comm;
  int i = 0;
  struct params *param_list;

  param_list = malloc (sizeof (struct params) * MAX_PARAMS);
  assert (param_list);

  while (fgets (buffer, 128, input))
    {
      assert (i < MAX_PARAMS);
      
      if ((comm = strstr (buffer, "#")) != NULL)
        {

          buffer[comm - buffer] = (comm - buffer) ? '\n' : '\0';
          buffer[comm - buffer + 1] = '\0';
        }

      if (buffer[0] != '\n' && buffer[0] != '\0')
        {
          sscanf (buffer, "%s %lf", &key[0], &value);

          strncpy (param_list[i].key, key, 24UL);
          param_list[i].value = value;
          i++;
        }
    }

  return param_list;
}

double
param_list_lookup (char *key, struct params *param_list)
{
  double value = 0;
  int i;
  for (i = 0; i < MAX_PARAMS; i++)
    {
      if (strcmp (key, param_list[i].key) == 0)
        {
          value = param_list[i].value;
          break;
        }
    }
 
  return value;
}

void
param_list_print (FILE *outfile, struct params *param_list)
{
  int i = 0;
  while (param_list[i].key[0] != '\0')
    {
      fprintf (outfile, "## %s = %f\n", param_list[i].key, param_list[i].value);
      i++;
    }
}

void
param_list_destroy (struct params *param_list)
{
  free (param_list);
}

/* READING DATA */

double *
read_file (char *file, size_t *row, size_t *column)
{
  const char delim[] = " \t";
  char *token;
  char *linebuffer;
  int i = 0;
  char *ret;
  size_t rows = 0, columns = 0;
  double *data;
  FILE *fp;

  fp = fopen (file, "r");
  assert (fp);

  linebuffer = malloc (MAX_LINE_LENGTH * sizeof (char));
  ret = fgets (linebuffer, MAX_LINE_LENGTH, fp);

  /* COUNTING LINES */

  while (ret)
    {
      token = strtok (linebuffer, delim);
      if (token[0] != '#' && token[0] != '\n' && token[0] != ' '
          && token[0] != '\t')
        {
          rows++;
        }
      ret = fgets (linebuffer, MAX_LINE_LENGTH, fp);
    }

  /* COUNTING COLUMNS */

  fseek (fp, 0, SEEK_SET);
  ret = fgets (linebuffer, MAX_LINE_LENGTH, fp);

  while (ret)
    {
      token = strtok (linebuffer, delim);
      while (token)
        {

          if (token[0] != '#' && token[0] != '\n' && token[0] != ' '
              && token[0] != '\t')
            {
              columns++;
            }
          else
            break;
          token = strtok (NULL, delim);
        }
      if (columns)
        break;
      ret = fgets (linebuffer, MAX_LINE_LENGTH, fp);
    }

  fseek (fp, 0, SEEK_SET);

  /* LOADING DATA INTO AN ARRAY */

  data = malloc (rows * columns * sizeof (double));
  assert (data);

  ret = fgets (linebuffer, MAX_LINE_LENGTH, fp);

  while (ret)
    {
      token = strtok (linebuffer, delim);
      while (token)
        {
          if (token[0] != '#' && token[0] != '\n' && token[0] != ' '
              && token[0] != '\t')
            {
              data[i++] = atof (token);
            }
          else
            break;
          token = strtok (NULL, delim);
        }
      ret = fgets (linebuffer, MAX_LINE_LENGTH, fp);
    }

  free (linebuffer);
  fclose (fp);

  *row = rows;
  *column = columns;

  return data;
}

int
get_size (const double *D, size_t r_D, size_t c_D, size_t *nX, size_t *nY)
{

  size_t n_K = 1, n_X;

  size_t i = 0;
  while (D[i * c_D] == D[(i + 1) * c_D] && i < r_D)
    {
      n_K++;
      i++;
    }

  *nY = n_K;

  n_X = r_D / n_K;

  *nX = n_X;

  return 0;
}

int
splice (const double *D, size_t c_D, size_t nX, size_t nY, double *K,
        double *V)
{
  size_t i, j, l;

  for (i = 0; i < nY; i++)
    {
      K[i] = D[i * c_D + 1];
    }

  for (i = 0; i < nX; i++)
    {
      for (j = 0; j < nY; j++)
        {
          for (l = 0; l < c_D - 2; l++)
            {
              V[j + i * nY + l * nX * nY] = D[c_D * (i * nY + j) + 2 + l];
            }
        }
    }

  return 0;
}
