CC=gcc
FF=gfortran
CFLAGS=-std=c89 -march=native
OPT=-O3
LDFLAGS=-march=native -lm -lgfortran
CSOURCES=$(wildcard */*.c)
CHEADERS=$(wildcard */*.h)
FSOURCES=$(wildcard */*.f)
MAIN=$(wildcard *.c)
OUT=$(patsubst %.c,%.out,$(MAIN))

ALL: $(COBJECTS) $(FOBJECTS) $(MAIN)
	$(CC) $(CFLAGS) $(OPT) $(MAIN) $(CSOURCES) $(FSOURCES) $(LDFLAGS) -o $(OUT)
	rm -rf *.o */*.o *.mod */*.mod
