#include <stdio.h>
#include <assert.h>
#include "lib_srg/lib_srg.h"
#include "lib_io/lib_io.h"

int main(int argc, char** argv)
{

  double *D, *V, *k, *wt, *mesh, h, lam;
  size_t nrow, ncol, nx, ny, nk, nmrow, nmcol, nv, i, j;
  char pot_out[100];
  FILE *pot_file, *param_file;
  struct params *param_list;

  param_file  = fopen(argv[1], "r");
  param_list = param_list_create(param_file);
  fclose(param_file);

  h = param_list_lookup("RK4-STEP-SIZE", param_list);
  lam = param_list_lookup("SRG-CUTOFF", param_list);
   
  sprintf(pot_out, "%s_lam%.2lf.srg", argv[2], lam);
  pot_file  = fopen(pot_out, "w");

  param_list_print(pot_file, param_list);
  param_list_destroy(param_list);

  fprintf(pot_file, "##     K                  K'             V(K,K')\n");	 

  D = read_file(argv[2], &nrow, &ncol);
  assert(ncol == 3); 
  get_size(D, nrow, ncol, &nx, &ny);

  if(nx == ny)
    nk = nx;

  nv = nk * nk * (ncol - 2); 
  
  k = malloc(nk * sizeof(double));
  wt = malloc(nk * sizeof(double));
  V = malloc(nv * sizeof(double));
  
  splice(D, ncol, nx, ny, k, V);
  free(D);

  mesh = read_file(argv[3], &nmrow, &nmcol);

  if(fabs(mesh[20] - k[10]) > 1E-10)
    {
      printf("Incompatible mesh file!! Exiting..\n");
      exit(-1);
    }
  if(nk != nmrow)
    {
      printf("Incompatible mesh file!! Size mismatch. Exiting..\n");
      exit(-1);
    }

  for(i = 0; i < nk; i++)
    {
      wt[i] = mesh[i * 2 + 1];
    }

  free(mesh);

  srg(lam, h, V, nv, k, wt);

  for(i = 0; i < nk; i++)
    {
     for(j = 0; j < nk; j++)
       {
	 fprintf(pot_file, "%+.15E %+.15E %+.15E\n", k[i], k[j], V[i * nk + j]);
       }
     fprintf(pot_file, "\n");
    }

  fclose(pot_file);
  
  free(k);
  free(wt);
  free(V);

  printf("\nPOTENTIAL WRITTEN TO '%s' \n", pot_out);
  
  return 0;
}
