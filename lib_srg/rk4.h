#include <stddef.h>

double rk4 ( double t0, double u0, double dt, double f ( double t, double u ) );
double *rk4vec ( double t0, size_t m, double u0[], double dt, 
		 double *f ( double t, size_t m, double u[], void *param1, void *param2 ), void *param1, void *param2 );

