#include <stdio.h>
#include "lib_srg.h"
#include "rk4.h"

#define PI 3.14159265359

double* wegner_flow(double s, size_t nv, double *v_bare, void *param1, void *param2)
{

double *k, *wt, *v_evol, prefac;
size_t nk, i, j, q;

  s = 0;   /* To suppress warning */
  k = param1;
  wt = param2;
  nk = sqrt(nv);
  v_evol = calloc(nv, sizeof(double));
  prefac = (2 / PI);
  
  for (i = 0; i < nk; i++)
    {
    for (j = 0; j < nk; j++)
      {
        for (q = 0; q < nk; q++)
          {
            double v_iq, v_qj;
            v_iq = v_bare[i * nk + q];
            v_qj = v_bare[q * nk + j];
            
            v_evol[i * nk + j] += k[q] * k[q] * wt[q]
	                               * (k[i] * k[i] + k[j] * k[j] - 2 * k[q] * k[q])
	                               * (v_iq * v_qj);
            
          }
       
        v_evol[i * nk + j] += -prefac * v_bare[i * nk + j]             
	                              * ( (k[i] + k[j]) * (k[i] - k[j]) )	
                                      * ( (k[i] + k[j]) * (k[i] - k[j]) );  
	
      }
    }

  return v_evol;
}

int srg(double lam, double h, double *v, size_t nv, double *k, double *wt )
{
  double s,
    s_start = h,
    s_end   = 1 / (lam * lam * lam * lam);
  size_t i;
  
  for (s = s_start; s < s_end; s = s + h)
    {
      printf("\rs = %+.15E", s);
      double *dv = rk4vec(s_start, nv, v, h, wegner_flow, k, wt);
      assert(dv);
      
      for(i = 0; i < nv; i++)
	{
	  v[i] = dv[i];
	}
      assert(i == nv);
      
      free(dv);
    }

  return 0;
}
