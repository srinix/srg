#include <stdlib.h>
#include <stddef.h>
#include <math.h>
#include <assert.h>

double *wegner_flow(double s, size_t n_k, double *v_bare, void *param1, void *param2);
int srg(double lambda, double h, double *v, size_t nv, double *k, double *wt );
